<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EncodePasswordSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function encodePassword(ViewEvent $event)
    {
        // get the name of controller
        $result = $event->getControllerResult();
        //get the methode == post
        $methode = $event->getRequest()->getMethod();

        if($methode === "POST" && $result instanceof User){
            // recupere le passord ensuite le hashe avec func encodePassword
            $hash = $this->encoder->encodePassword($result, $result->getPassword());
            $result->setPassword($hash);

        }
        // ...
    }

    public static function getSubscribedEvents()
    {
        return [
           KernelEvents::VIEW => ["encodePassword", EventPriorities::PRE_WRITE]
        ];
    }
}
